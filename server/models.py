from .lib import QueryAPI, NeighborsManager, ManyToManyManager
from . import db

class Lot(QueryAPI):
    arc = None
    _right = None
    _left = None
    _front = None
    _behind = None

    def __init__(self, address):
        self.address = address
        self.neighbours = NeighborsManager(self)
        self.materials = ManyToManyManager(self, Material)

    def __repr__(self):
        return '<Lot: {}>'.format(self.address.replace('/n', ' '))

    @property
    def left(self):
        return self._left

    @left.setter
    def left(self, value):
        self._left = value

    @property
    def right(self):
        return self._right

    @right.setter
    def right(self, value):
        self._right = value

    @property
    def behind(self):
        return self._behind

    @behind.setter
    def behind(self, value):
        self._behind = value

    @property
    def front(self):
        return self._front

    @front.setter
    def front(self, value):
        self._front = value

class Plan(QueryAPI):
    def __init__(self, label, elevation):
        self.label = label
        self.elevation = elevation
        self.rules = ManyToManyManager(self, Rule)


class Elevation(QueryAPI):
    def __init__(self, label):
        self.label = label
        self.rules = ManyToManyManager(self, Rule)


class Style(QueryAPI):
    def __init__(self, label):
        self.label = label
        self.rules = ManyToManyManager(self, Rule)


class ARC(QueryAPI):
    materials = []

    def __init__(self, lot, price):
        lot.add_arc(self)
        self.lot = lot
        self.price = price
        self.rules = ManyToManyManager(self, Rule)

    def add_material(self, material):
        self.materials.append(material)


class Material(QueryAPI):
    def __init__(self, name, manufacturer, category):
        self.name = name
        self.manufacturer = manufacturer
        self.category = category
        self.rules = ManyToManyManager(self, Rule)


class Project(QueryAPI):
    def __init__(self, name):
        self.name = name

class Rule(QueryAPI):
    def __init__(self, side, operation, quantity):
        self.side = side
        self.operation = operation
        self.quantity = quantity
