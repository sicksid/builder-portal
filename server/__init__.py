import os
from flask import Flask
from flask_orator import Orator
from lib import config

db = Orator()
def create_app():
    app = Flask(__name__)
    environment = os.environ['FLASK_ENV']
    config_object = config[environment]
    if environment == 'production':
        config_object['ORATOR_DATABASES'] = {
            'default': 'production',
            'production': {
                'driver': 'mysql',
                'host': os.environ.get('DATABASE_HOST'),
                'database': os.environ.get('DATABASE_NAME'),
                'user': os.environ.get('DATABASE_USER'),
                'password': os.environ.get('DATABASE_PASSWORD')
            }
        }

    app.config.from_object(config_object)
    db.init_app(app)
    return app
