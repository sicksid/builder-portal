# from shapely.geometry import Polygon, shape
from pyproj import Proj, transform
import shapefile


class ShapefileImporter(object):
    def __init__(self, path):
        self.reader = shapefile.Reader(path)
        self.errors = []
        self.fields = [field[0] for field in self.reader.fields[1:]]

    @property
    def data(self):
        return dict(
            type="FeatureCollection",
            features=filter(lambda feature: feature, [
                self.generate_feature(feature)
                for feature in self.extract_lots(self.reader.shapeRecords())
            ])
        )

    def transform_coords(self, coordinates):
        x, y = coordinates
        return transform(Proj(init='epsg:4326'), Proj(init='epsg:3857'), x, y)

    def extract_lots(self, features):
        return filter(
            lambda feature: dict(
                zip(self.fields, feature.record)).get('Layer') == 'LOTS', features)

    def generate_feature(self, feature):
        properties = dict(zip(self.fields, feature.record))
        try:
            geometry = feature.shape.__geo_interface__
            coordinates = [
                coords# self.transform_coords(coords) # use this to transform coordinates from epsg reference systems
                for coords in geometry['coordinates']
            ]
            # geometry['type'] = 'Polygon'
            geometry['coordinates'] = coordinates
        except Exception, exception:
            self.errors.append(exception)
            return None

        return dict(
            type="Feature",
            geometry=geometry,
            properties=properties
        )
