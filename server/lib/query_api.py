from uuid import uuid4
from .storage_backend import StorageBackend


class QueryAPI(object):
    storage_backend = StorageBackend
    queryset = None

    def __init__(self, *args, **kwargs):
        super(QueryAPI, self).__init__(*args, **kwargs)

    @property
    def storage_key(self):
        return self.__class__.__name__.lower()

    @property
    def storage(self):
        return self.storage_backend.init(self.storage_key)

    @storage.setter
    def storage(self, value):
        self.storage_backend.commit(self.storage_key, value)

    def save(self):
        if not hasattr(self, 'id'):
            self.id = uuid4().hex
        self.storage = self

    def delete(self):
        self.storage_backend.commit(self.storage_key, self, remove=True)

    @classmethod
    def exclude(cls, prop, values):
        queryset = cls.all().queryset
        return filter(lambda entity: getattr(entity, prop) not in values, queryset)

    @classmethod
    def filter(cls, prop, value):
        if not cls.queryset or len(cls.queryset):
            key = cls.__name__.lower()
            queryset = cls.storage_backend.init(key)
        else:
            queryset = cls.queryset

        cls.queryset = filter(
            lambda entity: getattr(entity, prop, None) == value,
            queryset
        )

        return cls

    @classmethod
    def get(cls, prop, value):
        queryset = cls.filter(prop, value).queryset
        if not len(queryset) > 0:
            return None

        return queryset[0]

    @classmethod
    def all(cls):
        key = cls.__name__.lower()
        cls.queryset = cls.storage_backend.init(key)
        return cls

    @classmethod
    def exists(cls, property):
        key = cls.__name__.lower()
        queryset = cls.storage_backend.init(key)
        return filter(lambda entity: hasattr(entity, property), queryset)
