from flask_dotenv import DotEnv


class Config(object):

    @classmethod
    def init_app(self, app):
        env = DotEnv()
        env.init_app(app)

class DevelopmentConfig(Config):
    DEBUG = True
    TESTING = False
    ORATOR_DATABASES = {
        'default': 'development',
        'development': {
            'driver': 'sqlite',
            'database': ':memory:'
        }
    }

class ProductionConfig(Config):
    DEBUG = True
    TESTING = False

class TestConfig(Config):
    DEBUG = True
    TESTING = True
    ORATOR_DATABASES = {
        'default': 'test',
        'test': {
            'driver': 'sqlite',
            'database': ':memory:'
        }
    }

config = {
    'development': DevelopmentConfig,
    'test': TestConfig,
    'production': ProductionConfig
}
