from backports.tempfile import TemporaryDirectory
from subprocess import call as execute
from os.path import abspath, basename
from os.path import split as path_split
from os.path import join as path_join


class DxfConverter(object):
    command = "ogr2ogr -nlt polygon -f \"Esri Shapefile\" \"{output}\" \"{input}\" -skipfailures"

    def __init__(self, path):
        self.input = abspath(path)
        filename = basename(self.input)
        self.output_filename = filename.replace('dxf', 'shp')
        with TemporaryDirectory() as tempdir:
            output = path_join(tempdir, self.output_filename)
            result = execute(self.command.format(input=self.input, output=output), shell=True)
            if result == 0:
                self.output = open(output, 'r+')
            else:
                raise Exception("Error writing file")

    def store(self, path):
        with open(path_join(path, self.output_filename), 'w+') as f:
            f.write(self.output.read())
            self.output.close()
            self.output = None
