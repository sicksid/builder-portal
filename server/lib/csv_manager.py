from backports.tempfile import TemporaryDirectory
from os.path import join as path_join


class CSVManager(object):
    __model__ = None

    def __init__(self, model):
        if not model:
            raise Exception("Did not provided a Model")

        self.model = model

    @property
    def model(self):
        return self.__model__

    @model.setter
    def model(self, value):
        self.__model__ = value

    def validate_initialized(self):
        if not self.model:
            raise Exception("Not initialized")

    def unload(self, fields=None, conditions=None):
        if not fields:
            fields = []

        if not conditions:
            conditions = []

        self.validate_initialized()

        records = self.model.all().queryset
        for condition in conditions:
            records = records.filter(*condition)

        csv = [fields]

        for record in records:
            data = []
            for field in fields:
                if not hasattr(record, field):
                    continue
                data.append(getattr(record, field))
            csv.append(data)

        return csv

    def load(self, fields, csv):
        self.validate_initialized()

        header = None
        for index, row in enumerate(csv):
            if index == 0:
                header = row
                continue

            data = dict(zip(header, row))

            if not data.get('id'):
                continue

            record = self.model.get('id', data['id'])
            for field in fields:
                setattr(record, field, data[field])

            record.save()
