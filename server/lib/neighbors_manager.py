from .query_api import QueryAPI

class NeighborsManager(QueryAPI):
    def __init__(self, origin, **options):
        self.origin = origin
        self.max_right = options.get('max_right', 4)
        self.max_left = options.get('max_left', 4)
        self.max_front = options.get('max_front', 4)
        self.max_behind = options.get('max_behind', 0)

    @property
    def data(self):
        data = {}
        for side in ['front', 'right', 'behind', 'left']:
            data[side] = self.search_neighbors(side)
        return data

    def tolist(self):
        return self.data.values()

    def search_neighbors(self, side):
        current = self.origin
        neighbors = []
        count = 0
        while count < getattr(self, 'max_{}'.format(side), 0):
            lot = getattr(current, side, None)
            if lot and not any(neighbor
                               for neighbor in neighbors
                               if neighbor.address == lot.address or
                               lot.address == self.origin.address):
                neighbors.append(lot)
                current = lot

            count = count + 1
        return neighbors

    def add(self, side, neighbor):
        setattr(self.origin, side, neighbor)

        if side == 'left':
            setattr(neighbor, 'right', self.origin)
        elif side == 'right':
            setattr(neighbor, 'left', self.origin)
        else:
            setattr(neighbor, side, self.origin)
