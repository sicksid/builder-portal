class StorageBackend(object):
    storage = {}

    @classmethod
    def init(cls, key):
        if not cls.storage.get(key):
            cls.storage[key] = []

        return cls.storage[key]

    @classmethod
    def commit(cls, key, value, remove=False):
        if not cls.storage.get(key):
            cls.init(key)
        cls.storage[key] = filter(
            lambda item: item.id != value.id, cls.storage[key])

        if not remove:
            cls.storage[key].append(value)
