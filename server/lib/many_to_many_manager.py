from query_api import QueryAPI


class ManyToManyManager(QueryAPI):
    def __init__(self, model, resource):
        self.model = model
        self.resource = resource

    @property
    def model_id_key(self):
        return "%s_id" % self.model.__class__.__name__.lower()

    @property
    def queryset(self):
        return self.resource.filter(self.model_id_key, self.model.id)

    def add(self, entity):
        setattr(entity, self.model_id_key, self.model.id)
        entity.save()

    def all(self):
        results = self.queryset
        return results.queryset

    def filter(self, prop, value):
        return self.queryset.filter(prop, value)

    def get(self, prop, value):
        queryset = filter(lambda entity: getattr(entity, prop, None) == value, self.all())
        if not queryset:
            return None
        return queryset[0]

    def remove(self, entity):
        setattr(entity, self.model_id_key, None)
        entity.save()
