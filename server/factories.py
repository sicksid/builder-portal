from faker import Faker
from models import *
from random import choice, randint
import factory

fake = Faker()

class LotFactory(factory.Factory):
    class Meta:
        model = Lot

    address = factory.LazyAttribute(lambda o: fake.address())

class RuleFactory(factory.Factory):
    class Meta:
        model = Rule

    side = factory.LazyAttribute(lambda o: choice(['left', 'right', 'behind', 'front']))
    operation = factory.LazyAttribute(
        lambda o: choice(['<', '<=', '>', '>=', '='])
    )
    quantity = factory.LazyAttribute(lambda o: randint(0, 4))
    

class PlanFactory(object):
    pass

class ElevationFactory(object):
    pass

class StyleFactory(object):
    pass

class ARCFactory(object):
    pass


class MaterialFactory(factory.Factory):
    class Meta:
        model = Material

    name = factory.LazyAttribute(lambda o: fake.color_name())
    category = factory.LazyAttribute(lambda o: fake.word())
    manufacturer = factory.LazyAttribute(lambda o: fake.company())
