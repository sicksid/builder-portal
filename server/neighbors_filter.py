import json, time
import pandas as pd
import numpy as np
from django.conf import settings
from math import sqrt
from scipy.spatial import distance
from django.core.cache import cache
from sklearn.neighbors import NearestNeighbors
from api.serializers import RecordsSerializer


class NeighborsFilter(object):
    def __init__(self, lot_id, project_id):
        records = RecordsSerializer(project_id, split_points=True).serialize
        self.project_id = project_id
        self.lots = pd.DataFrame(records['features'])
        self.lots = pd.DataFrame(list(self.lots['properties'])).set_index('id').reset_index()
        self.lot_id = self.lots[self.lots['ad_id'] == lot_id].index.values[0]
        self.lots['fc_x'] = (self.lots['fr_x'] + self.lots['fl_x']) / 2
        self.lots['fc_y'] = (self.lots['fr_y'] + self.lots['fl_y']) / 2


    @property
    def serialize(self):
        cache_key = "neighbors/%s/project/%s" % (self.lot_id, self.project_id)
        if cache.get(cache_key) and not settings.TESTING:
            return json.loads(cache.get(cache_key))
        else:
            data = self.get_neighbors()
            cache.set(cache_key, json.dumps(data), 86400)
            return data

    def get_neighbors(self):
        centers = self.lots[['fc_x', 'fc_y']].values
        nbrs = NearestNeighbors(n_neighbors=15).fit(centers)
        distances, indices = nbrs.kneighbors(centers)
        idx = self.lots.iloc[self.lot_id].name
        neighbors = indices[idx]
        neighbors = self.remove_neighbors_behind(self.lots.iloc[neighbors])
        neighbors = [self.lots[self.lots['ad_id'] == lot].index.values[0] for lot in neighbors]
        neighbors = self.minimize_lots(self.lots.iloc[ neighbors])
        return {
            "sides": self.lots.iloc[neighbors['sides']]['ad_id'].values.tolist(),
            "across": self.lots.iloc[neighbors['across']]['ad_id'].values.tolist()
        }

    def remove_neighbors_behind(self, lots):
        lots = lots.reset_index(drop=True)
        rear_boundary = lots.iloc[0][['rl_x', 'rl_y', 'rr_x', 'rr_y']].values
        neighbors = []

        for idx, row in lots.iterrows():
            segment = row[['rl_x', 'rl_y', 'rr_x', 'rr_y']].values
            if not self.segment_intersection(rear_boundary, segment):
                neighbors.append(row['ad_id'])

        return neighbors


    def segment_intersection(self, segment1, segment2):
        A = segment1[[0, 1]]
        B = segment1[[2, 3]]
        C = segment2[[0, 1]]
        D = segment2[[2, 3]]

        return self.counter_clock(A, C, D) != self.counter_clock(B, C, D) and self.counter_clock(A, B, C) != self.counter_clock(A, B, D)

    def counter_clock(self, a, b, c):
        return (c[1] - a[1]) * (b[0] - a[0]) > (b[1] - a[1]) * (c[0] - a[0])

    def minimize_lots(self, lots):
        side = 6
        front = 8
        less = []
        greater = []
        base_direction = lots.iloc[0]['direction']
        center = lots.iloc[0][['fc_x', 'fc_y']].values
        for idx, row in lots.iterrows():
            direction = row['direction']
            point = row[['fc_x', 'fc_y']].values
            if abs(direction - base_direction) <= 75:
                less.append(row.name)
            else:
                if self.distance(center, point) < 0.6:
                    greater.append(row.name)
        neighbors = less[:side+1] + greater[:front]

        return {
            "sides": less[:side+1][1:],
            "across": greater[:side]
        }

    def distance(self, one, two):
        return sqrt((one[0] - two[0]) ** 2 + (one[1] - two[1]) ** 2) * 1000

