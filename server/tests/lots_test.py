from unittest import TestCase
from ..models import Lot
from ..factories import LotFactory


class TestLots(TestCase):
    def test_add_neighbors(self):
        origin = LotFactory()
        right = LotFactory()
        left = LotFactory()
        behind = LotFactory()
        front = LotFactory()

        origin.neighbours.add('right', right)
        origin.neighbours.add('left', left)
        origin.neighbours.add('front', front)
        origin.neighbours.add('behind', behind)

        self.assertEqual(origin.right.address, right.address)
        self.assertEqual(origin.left.address, left.address)
        self.assertEqual(origin.front.address, front.address)
        self.assertEqual(origin.behind.address, behind.address)

        self.assertEqual(left.right.address, origin.address)
        self.assertEqual(right.left.address, origin.address)
        self.assertEqual(front.front.address, origin.address)
        self.assertEqual(behind.behind.address, origin.address)
