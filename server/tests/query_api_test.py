from unittest import TestCase
from ..lib import QueryAPI


class TestQueryAPI(TestCase):
    def setUp(self):
        QueryAPI.storage_backend.storage = {}
        self.query_api = QueryAPI()

    def test_storage_key(self):
        self.assertEqual(self.query_api.storage_key, 'queryapi')

    def test_storage_getter(self):
        self.assertIsInstance(self.query_api.storage, list)

    def test_storage_setter(self):
        self.query_api.storage = 'item'
        self.assertEqual(self.query_api.storage, ['item'])

    def test_save(self):
        class FakeModel(QueryAPI):
            pass

        instance = FakeModel()
        instance.save()
        self.assertIsNotNone(instance.id)

    def test_filter(self):
        class FakeModel(QueryAPI):
            pass

        instance = FakeModel()
        instance.save()

        instance2 = FakeModel()
        instance2.save()

        instance3 = FakeModel()
        instance3.save()
        results = FakeModel.filter('id', instance.id)
        self.assertEqual(len(results.queryset), 1)

    def test_get(self):
        class FakeModel(QueryAPI):
            pass

        instance = FakeModel()
        instance.save()
        result = FakeModel.get('id', instance.id)
        self.assertEqual(result.id, instance.id)

    def test_all(self):
        class FakeModel(QueryAPI):
            pass

        instance = FakeModel()
        instance.save()

        instance2 = FakeModel()
        instance2.save()

        instance3 = FakeModel()
        instance3.save()
        results = FakeModel.all()
        self.assertEqual(len(results.queryset), 3)

    def test_exclude(self):
        class FakeModel(QueryAPI):
            pass

        instance = FakeModel()
        instance.save()

        instance2 = FakeModel()
        instance2.save()

        instance3 = FakeModel()
        instance3.save()

        self.assertEqual(FakeModel.exclude(
            'id', [instance2.id, instance3.id])[0].id, instance.id)
