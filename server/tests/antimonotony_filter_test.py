from unittest import TestCase
from ..lib import AntimonotonyFilter
from ..models import Lot, Material, Rule
from ..factories import LotFactory, MaterialFactory, RuleFactory


class TestAntimonotonyFilter(TestCase):
    def setUp(self):
        self.lot = LotFactory()
        self.lot.save()
        lot = LotFactory()
        lot.save()
        self.lot.neighbours.add('right', lot)
        tmp_lot = LotFactory()
        tmp_lot.save()
        lot.neighbours.add('right', tmp_lot)
        lot = tmp_lot
        tmp_lot = LotFactory()
        tmp_lot.save()
        lot.neighbours.add('right', tmp_lot)

        lot = LotFactory()
        lot.save()
        self.lot.neighbours.add('left', lot)
        tmp_lot = LotFactory()
        tmp_lot.save()
        lot.neighbours.add('left', tmp_lot)
        lot = tmp_lot
        tmp_lot = LotFactory()
        tmp_lot.save()
        lot.neighbours.add('left', tmp_lot)

        lot = LotFactory()
        lot.save()
        self.lot.neighbours.add('front', lot)
        tmp_lot = LotFactory()
        tmp_lot.save()
        lot.neighbours.add('front', tmp_lot)
        lot = tmp_lot
        tmp_lot = LotFactory()
        tmp_lot.save()
        lot.neighbours.add('front', tmp_lot)

        self.materials = []
        for material in range(6):
            material = MaterialFactory()
            material.save()
            self.materials.append(material)

    def tearDown(self):
        for material in self.materials:
            material.delete()

    def test_initialize(self):
        am_filter = AntimonotonyFilter(Material, self.lot)
        self.assertEqual(am_filter.lot.id, self.lot.id)
        self.assertEqual(am_filter.resource.__class__.__name__, Material.__class__.__name__)

    def test_resource_name_property(self):
        am_filter = AntimonotonyFilter(Material, self.lot)
        self.assertEqual(am_filter.resource_name, Material.__name__.lower())

    def test_filter_does_not_return_material(self):
        rule = Rule('left', '>', 3)
        material = self.materials[0]
        material.rules.add(rule)
        left_neighbours = self.lot.neighbours.data['left']
        left_neighbours[2].materials.add(material)
        am_filter = AntimonotonyFilter(Material, self.lot)
        materials = am_filter.filter()
        self.assertEqual(len(materials), 6)

    def test_filter_does_return_material(self):
        rule = Rule('left', '<', 3)
        material = self.materials[0]
        material.rules.add(rule)
        left_neighbours = self.lot.neighbours.data['left']
        left_neighbours[2].materials.add(material)
        am_filter = AntimonotonyFilter(Material, self.lot)
        materials = am_filter.filter()
        self.assertEqual(len(materials), 5)


    def test_neighbours_property(self):
        am_filter = AntimonotonyFilter(Material, self.lot)
        self.assertEqual(am_filter.neighbours, self.lot.neighbours.data)

    def test_resources_is_empty(self):
        am_filter = AntimonotonyFilter(Material, self.lot)
        self.assertEqual(am_filter.resources(self.lot.neighbours.data['left']), [[], [], []])

    def test_resources_is_not_empty(self):
        left_neighbours = self.lot.neighbours.data['left']
        left_neighbours[0].materials.add(self.materials[0])
        am_filter = AntimonotonyFilter(Material, self.lot)
        self.assertEqual(am_filter.resources(
            left_neighbours), [[self.materials[0]], [], []])
