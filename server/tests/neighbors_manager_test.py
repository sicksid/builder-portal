from unittest import TestCase
from ..factories import LotFactory
from ..lib import NeighborsManager

class TestNeighborsManager(TestCase):
    def setUp(self):
        self.origin = LotFactory()
        self.right = LotFactory()
        self.left = LotFactory()
        self.behind = LotFactory()
        self.front = LotFactory()

    def test_add(self):
        manager = NeighborsManager(self.origin)
        manager.add('right', self.right)
        self.assertEqual(self.origin.right.address, self.right.address)
        self.assertEqual(self.right.left.address, self.origin.address)
        self.assertIsNone(self.origin.left)

    def test_tolist(self):
        manager = NeighborsManager(LotFactory())
        manager.add('right', LotFactory())
        self.assertIsInstance(manager.tolist(), list)

    def test_data(self):
        lot = LotFactory()
        manager = NeighborsManager(lot)
        manager.add('right', LotFactory())
        manager.add('left', LotFactory())
        manager.add('front', LotFactory())
        manager.add('behind', LotFactory())
        self.assertIsInstance(manager.data, dict)
        self.assertIsInstance(manager.data['right'], list)
        self.assertEqual(len(manager.data['right']), 1)
        self.assertIsInstance(manager.data['left'], list)
        self.assertEqual(len(manager.data['left']), 1)
        self.assertIsInstance(manager.data['front'], list)
        self.assertEqual(len(manager.data['front']), 1)
        self.assertIsInstance(manager.data['behind'], list)
        self.assertEqual(len(manager.data['behind']), 0)

    def test_search_neighbors(self):
        manager = NeighborsManager(LotFactory())
        manager.add('right', LotFactory())
        second_lot = LotFactory()
        second_manager = NeighborsManager(manager.origin.right)
        second_manager.add('right', second_lot)
        third_lot = LotFactory()
        third_manager = NeighborsManager(second_lot)
        third_manager.add('right', third_lot)
        fourth_lot = LotFactory()
        fourth_manager = NeighborsManager(third_lot)
        fourth_manager.add('right', fourth_lot)
        data = manager.search_neighbors('right')
        self.assertIsInstance(data, list)
        self.assertEqual(len(data), 4)
