from unittest import TestCase
from ..lib import StorageBackend


class TestStorageBackend(TestCase):
    def test_init(self):
        StorageBackend.storage = {}
        StorageBackend.init('test')
        self.assertEqual(StorageBackend.storage.keys(), ['test'])

    def test_commit(self):
        StorageBackend.commit('test', {'random': 'item'})

        data = StorageBackend.init('test')

        self.assertEqual(data[0].keys(), ['random'])
        self.assertEqual(data[0].values(), ['item'])

    def test_commit_overwrites_record_when_id_is_present(self):
        from ..lib import QueryAPI

        StorageBackend.storage = {}

        class RandomClass(QueryAPI):
            name = 'random'
            value = 'item'

        random_item = RandomClass()
        random_item.save()

        StorageBackend.commit('randomclass', random_item)
        random_item.name = 'random 2'
        random_item.save()
        data = StorageBackend.init('randomclass')
        self.assertEqual(len(data), 1)

    def test_commit_removes_record(self):
        from ..lib import QueryAPI

        StorageBackend.storage = {}

        class RandomClass(QueryAPI):
            name = 'random'
            value = 'item to be removed'

        random_item = RandomClass()
        random_item.save()

        StorageBackend.commit('randomclass', random_item)
        StorageBackend.commit('randomclass', random_item, remove=True)
        data = StorageBackend.init('randomclass')
        self.assertEqual(len(data), 0)
