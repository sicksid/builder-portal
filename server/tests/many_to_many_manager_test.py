from unittest import TestCase
from ..lib import ManyToManyManager
from ..models import Lot, Material
from ..factories import LotFactory, MaterialFactory


class TestAntimonotonyFilter(TestCase):
    def setUp(self):
        self.materials = []
        for material in range(5):
            material = MaterialFactory()
            material.save()
            self.materials.append(material)

        self.lot = LotFactory()
        self.lot.save()

    def tearDown(self):
        for material in self.materials:
            self.lot.materials.remove(material)

    def test_initialize(self):
        pass

    def test_model_key_id_property(self):
        pass

    def test_filter(self):
        pass

    def test_all(self):
        pass

    def test_add(self):
        self.lot.materials.add(self.materials[0])
        material = self.lot.materials.get('id', self.materials[0].id)
        self.assertEqual(self.materials[0].id, material.id)

    def test_remove(self):
        self.lot.materials.add(self.materials[0])
        self.lot.materials.remove(self.materials[0])
        material = self.lot.materials.get('id', self.materials[0].id)
        self.assertIsNone(material)
